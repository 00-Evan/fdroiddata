Categories:Games
License:GPLv3
Web Site:
Source Code:https://github.com/queler/holokenmod
Issue Tracker:https://github.com/queler/holokenmod/issues

Auto Name:HoloKenMod
Summary:KenKen game
Description:
Adding some bug fixes and features to [[com.tortuca.holoken]] which was: Based
on KenKen and forked from [[net.cactii.mathdoku]] v1.9, now featuring a
Holo-inspired design and more user-friendly functionalities.

Also known as Mathdoku or Kendoku, this KenKen game will test your mental
arithmetic and challenge your mind. The goal is to fill in the entire grid with
numbers while meeting the operation restrictions for each grid cage. No
description available
.

Repo Type:git
Repo:https://github.com/queler/holokenmod

Build:1.3,13
    commit=889ab1f3d8349efdf93e7fa6b0abce49a0551592
    target=android-22

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.3
Current Version Code:13
